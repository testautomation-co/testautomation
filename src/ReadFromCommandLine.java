import java.util.Scanner;

public class ReadFromCommandLine {

	public static void main(String[] args) {
		int firstnumber;
		int secondnumber;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter first number:");
		firstnumber = scanner.nextInt();
		System.out.println("Enter second number:");
		secondnumber = scanner.nextInt();

		int sum = firstnumber + secondnumber;

		System.out.println("The sum of two numbers is " + sum);

	}

}