package dr;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class drRadio {
	WebDriver driver;

	@Test
	public void f() {
		driver.get("https://testautomation.co/webelements/");
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		List<WebElement> browserradios  = driver.findElements(By.name("browser"));
		System.out.println(browserradios.size());
		browserradios.get(1).click();
		for(WebElement radio:browserradios)
		{
			System.out.println(radio.getAttribute("value"));
			if(radio.getAttribute("value").equals("safari"))
				radio.click();
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();

	}

}
