package dr;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Assertions {
	WebDriver driver;

	@BeforeMethod
	public void setupTest() {
		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testHardAssertion() {
		String myStringobject = null;
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		
		Assert.assertEquals(driver.getTitle(), "Google");
		
		System.out.println(driver.getCurrentUrl());
		WebElement searchBox = driver.findElement(By.xpath("//input[@title='Search' and @type='text']"));
		searchBox.sendKeys("TestAutomation.co");
		WebElement submitSearchBtn = driver.findElement(By.name("btnK"));
		submitSearchBtn.submit();
		System.out.println(driver.getTitle());
		//Assert.assertEquals(driver.getTitle(), "Google");
		Assert.assertNotEquals("", driver.getTitle());
		
		Assert.assertTrue(driver.getTitle().length() > 0);
		Assert.assertFalse(driver.getTitle().length() == 0);
		
		Assert.assertNull(myStringobject);
		myStringobject = "someNotNullValue";
		Assert.assertNotNull(myStringobject);
		System.out.println("hello world ! did you reach here?");
	}

	public void testSoftAssertion() {

	}

	@AfterMethod
	public void afterMethod() {
		// driver.quit();
	}

}
