package javafundamentals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class JavaFileIOExample {

	public static void main(String[] args) throws IOException {
		write2file();
		File f = new File("myfile.txt");
		readFromFile(f);

	}

	public static void write2file() throws IOException {
		File myfile = new File("myfile.txt");
		FileOutputStream fos = new FileOutputStream(myfile);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		bw.write("hello");
		bw.newLine();

		bw.close();

	}
	
	public static void readFromFile(File f) throws IOException
	{
		FileInputStream fis = new FileInputStream(f);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line;
        line = br.readLine();
        System.out.println("read from the file "  + line);
        br.close();
		
		
	}

}
