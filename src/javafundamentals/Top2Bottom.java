package javafundamentals;

import java.util.Date;

public class Top2Bottom {
	private int age;
	private String name;
	
	public static int multiply(int number1, int number2)
	{
		int product =0;
		product = number1 * number2;
		System.out.println("product is " + product);
		return product;
	}
	
	public static void main(String[] args) {
		int productvalue = multiply(2,3);
		System.out.println("printed from main" + productvalue);
		Date d = new Date();
		System.out.println(d.toString());

	}
	
	
	

}
