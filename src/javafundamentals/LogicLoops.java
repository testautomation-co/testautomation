package javafundamentals;

public class LogicLoops {

	public static void main(String[] args) {
		
		for(int person_age =10; person_age<=25; person_age++)
		{
			
			
			if (person_age >= 20) {
				System.out.println("Person is adult");
			}

			else if (person_age > 12 && person_age < 20) {
				System.out.println("Person is Teenager");
			}

			else {
				System.out.println("Age unknown");
			}
			
			
		}
	}
}
