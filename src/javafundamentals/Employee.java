package javafundamentals;

public class Employee {
	private String employeename;
	private int salary;
	private String department;

	public void setName(String name) {
		employeename = name;
	}

	public String getName() {
		return employeename;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}
	


	public void setDepartment(String department) {
		this.department = department;
	}

	
	public static void main(String[] args) {

		Employee emp = new Employee();
		emp.setName("John Doe");
		emp.setDepartment("IT");
		emp.setSalary(4000);
	
		System.out.println(emp.getName());
		System.out.println(emp.getDepartment());
		System.out.println(emp.getSalary());

		
		Employee emp1 = new Employee();
		emp1.setName("jane Doe");
		emp1.setDepartment("IT");
		emp1.setSalary(4000);
	}

}
