package javafundamentals;

/*
Conditions / Conditional Statements:
performs different computations or actions depending on whether a programmer-specified boolean condition evaluates to true or false.
*/
public class Logic {

	public static void main(String[] args) {

		int person_age = 19;

		if (person_age > 20) {
			System.out.println("Person is adult");
		}

		else if (person_age > 12 && person_age < 20) {
			System.out.println("Person is Teenager");
		}

		else {
			System.out.println("Age unknown");
		}

	}

}
