package javafundamentals;

public abstract class Animal {
	
	public void move()
	{
	 System.out.println("move");	
	}
	
	public void eat()
	{
		System.out.println("eat");
	}
	
	public void sleep()
	{
		System.out.println("sleep");
	}
	
	public  abstract void makesound();
	
	
	

}
