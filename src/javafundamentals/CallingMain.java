package javafundamentals;

public class CallingMain {
	
	public static void main(String[] args) {
		
		
		Dog dogObj = new Dog();
		dogObj.eat();
		dogObj.eat("pedigree");
		dogObj.move();
		dogObj.sleep();
	}

}
