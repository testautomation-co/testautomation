package javafundamentals;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListCollection {
	
	public static void main(String[] args) {
			EmployeeC empOne = new EmployeeC("John", "IT", 4000);
		EmployeeC empTwo = new EmployeeC("Kiran", "Finance", 5000);
		EmployeeC empThree = new EmployeeC("Amanda","Marketing", 7000);
		
		System.out.println(empOne.getName());
		System.out.println(empTwo.getName());
		System.out.println(empThree.getName());
		
		ArrayList<EmployeeC> ListOfEmployees = new ArrayList<EmployeeC>();
		ListOfEmployees.add(empOne);
		ListOfEmployees.add(empTwo);
		ListOfEmployees.add(empThree);
		
		
		Iterator<EmployeeC> iterator = ListOfEmployees.iterator();
		while(iterator.hasNext()) {
			System.out.println("inside List Iterator " + iterator.next().getName());
			
		}
		
		for(EmployeeC emp:ListOfEmployees)
		{
			
			System.out.println("inside enhanced for loop " + emp.getName());

			
		}
		
		
		
		


	}
	

}
