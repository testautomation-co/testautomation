package javafundamentals;

public class OperatorsDemo {
	
	public static void main(String[] args) {
		// arithmetic operation
		int x = 5;
		int y = 3;
		int z = 0;
		
		z = x + y;
		System.out.println("The value of z is " + z);
		// relational operation
		
		if(y > x)
		{
			System.out.println("y is larger number");
		}
		else
		{
			System.out.println("x is larger number");
		}
		
		// logical operation
		
		if(z > x && z > y)
		{
			System.out.println("z is the greatest number");
		}
		
		if(z > x || y > z)
		{
			System.out.println("z is the greatest number");
		}

		
		
		
		
	}


	
}
