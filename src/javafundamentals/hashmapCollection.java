package javafundamentals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class hashmapCollection {
	
	public static void main(String[] args) {
		
		HashMap<Integer, String> employeeIDNameMap = new HashMap<Integer, String>();
		
		employeeIDNameMap.put(100, "John Doe");
		employeeIDNameMap.put(101, "Jane Doe");
		employeeIDNameMap.put(102, "Raj Patel");
		
		Iterator ite = employeeIDNameMap.entrySet().iterator();
	
		while(ite.hasNext())
		{
		Map.Entry<Integer, String> keyvalue = (Entry<Integer, String>) ite.next();
		System.out.println(keyvalue.getKey() + keyvalue.getValue());
		}
		
		for(Map.Entry<Integer, String> entry : employeeIDNameMap.entrySet())
		{
			System.out.println(entry.getKey() + entry.getValue());
			
		}
		
	}

}
