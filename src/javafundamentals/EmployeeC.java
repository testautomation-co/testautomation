package javafundamentals;

public class EmployeeC {
	private String employeename;
	private int salary;
	private String department;
	
	
	public EmployeeC(String name, String dept, int sal)
	{
		System.out.println("I am in constructor of EmployeeC");
		this.employeename=name;
		this.salary=sal;
		this.department=dept;
	}

	private void setName(String name) {
		employeename = name;
	}

	public String getName() {
		return employeename;
	}

	public int getSalary() {
		return salary;
	}

	private void setSalary(int salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	private void setDepartment(String department) {
		this.department = department;
	}

	public static void main(String[] args) {
        EmployeeC emp = new EmployeeC("John Doe", "IT", 4000);
        EmployeeC emp1 = new EmployeeC("Jane " ,"IT", 4000);
		System.out.println(emp.getName());
		System.out.println(emp.getDepartment());
		System.out.println(emp.getSalary());

	}

}
