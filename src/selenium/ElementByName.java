package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class ElementByName {
	WebDriver driver = null;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testById() {
		driver.get("https://testautomation.co/webelements/");
        driver.findElement(By.name("yourname")).sendKeys("Mike");
        List<WebElement> names = driver.findElements(By.name("yourname"));
		names.get(1).sendKeys("Miky");
        
	}

	@AfterMethod
	public void afterMethod() {
		// driver.quit();

		
	}

}
