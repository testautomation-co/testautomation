package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class ElementByXpath {
	WebDriver driver = null;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testByXpath() {
		driver.get("https://testautomation.co/webelements/");
		//driver.findElement(By.xpath("//*[@id=\"formsubmit\"]/table/tbody/tr[6]/td[1]/input")).click();;
		driver.findElement(By.xpath("//input[@type=\"submit\"]")).click();
		
	}

	@AfterMethod
	public void afterMethod() {
		// driver.quit();

	}

}
